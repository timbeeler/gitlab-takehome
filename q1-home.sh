#!/bin/bash
# seven fields from /etc/passwd stored in $f1,f2...,$f7
#***************************************************************************************
#   Title: Understanding /etc/passwd File Format
#   Author: Vivek Gite
#   Date: August 2, 2017
#   Code version: <.1>
#   Availability: https://www.cyberciti.biz/faq/understanding-etcpasswd-file-format/
#***************************************************************************************
#	f1: username
#	f2: password
#	f3: UID
#	f4: GID
#	f5: finger
#	f6: home directory
#	f7: shell path

while IFS=: read -r f1 f2 f3 f4 f5 f6 f7
do 
 echo "$f1:$f6"
done < /etc/passwd
